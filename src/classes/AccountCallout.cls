/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class AccountCallout {
    private static final AuthenticationService authService = new AuthenticationService();
    private static final CustomSettingService customSetting = new CustomSettingService();
    @Future(Callout=true)
    public static void sendAll(List<Id> accountsIds) {
        // get fresh data by Ids
        final List<Account> accounts = Utils.getAccounts(accountsIds);
        // send a request
        final CredentialsDTO credentialsDTO = customSetting.getTargetOrgCredentials(Constants.TARGET_USER);
        final String accessToken = authService.getAccessToken(credentialsDTO);
        HttpResponse response = sendAccounts(accounts, accessToken);

    }

    private static HttpResponse sendAccounts(List<Account> accounts, String token) {

        final Http http = new Http();
        final HttpRequest request = new HttpRequest();
        request.setEndpoint(Constants.POST_ACCOUNTS_URL);
        request.setMethod('POST');
        request.setHeader('Authorization', 'Bearer ' + token);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('accept', 'application/json');

        JsonWrapper jsonWrapper = new JsonWrapper();
        jsonWrapper.accounts = accounts;
        request.setBody(JSON.serialize(jsonWrapper));

        final HttpResponse response = http.send(request);
        return response;
    }

    class JsonWrapper {
        public List<Account> accounts;
    }
}