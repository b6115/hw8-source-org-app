/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class AccountService implements IAccountService {

    public void sendAll(List<Account> accounts) {
        final List<Id> idsList = Utils.getIds(accounts);
        AccountCallout.sendAll(idsList);
    }

}