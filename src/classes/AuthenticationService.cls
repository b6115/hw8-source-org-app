/**
 * Created by alexanderbelenov on 29.05.2022.
 */

public with sharing class AuthenticationService {

    public String getAccessToken(CredentialsDTO credentials) {
        System.debug(Constants.AUTH_TOKEN_URL);
        System.debug(credentials.toString());

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(Constants.AUTH_TOKEN_URL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setBody(
                'grant_type=password' +
                        '&client_id=' + credentials.getClientId() +
                        '&client_secret=' + credentials.getClientSecret() +
                        '&username=' + credentials.getUsername() +
                        '&password=' + credentials.getPassword() + credentials.getSecurityToken()
        );
        HttpResponse response = http.send(request);
        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        System.debug(results);
        return (String)results.get('access_token');
    }
}