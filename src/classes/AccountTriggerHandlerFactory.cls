/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class AccountTriggerHandlerFactory implements ITriggerHandlerFactory {
    private static ITriggerHandler handler;
    public ITriggerHandler create() {
        if (handler == null) {
            handler = new AccountTriggerHandler();
        }
        return handler;
    }
}