/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class Constants {
    public static final String TARGET_ORG_URL = 'https://mindful-otter-9nbr7d-dev-ed.my.salesforce.com';
    public static final String POST_ACCOUNTS_URL = TARGET_ORG_URL + '/services/apexrest/accounts';
    public static final String AUTH_TOKEN_URL = 'https://login.salesforce.com/services/oauth2/token';
    public static final String TARGET_USER = 'Oleksandr Bielienov';
}

//'3MVG9Ud3b0C7KnniO8_vEqHbbUr1dNWnFCMFmEymKJzft0aaa.OPklEZOEId6SH9QAYM92bqa44XNwP.XfXDC',
//'9C24F2E3D447C337BD009F0EFAC5225BA9E3373755BFBD65842739C904F8AF34',
//'belenoff.s@mindful-otter-9nbr7d.com',
//'fUnkEr-60223986',
//'yOD8m64ZiwWVyAoVu1NkbpKqj'