/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public interface ITriggerHandlerFactory {
    ITriggerHandler create();
}