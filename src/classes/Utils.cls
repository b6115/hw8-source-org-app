/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public with sharing class Utils {

    public static List<Account> getAccounts(List<Id> ids) {
        final List<Account> accountList = [
                SELECT AccountNumber, AnnualRevenue, AccountSource,
                        BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry,
                        Description,
                        Fax,
                        Industry,
                        NumberOfEmployees, Name,
                        Rating,
                        Ownership,
                        Phone,
                        Site, Sic, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry,
                        Type, TickerSymbol,
                        Website
                FROM Account
                WHERE Id In :ids];
        return removeIds(accountList);
    }

    public static List<SObject> removeIds(List<SObject> objectList) {
        final List<SObject> withoutIdObjects = new List<SObject>();
        for (SObject element : objectList) {
            withoutIdObjects.add(element.clone(false));
        }
        return withoutIdObjects;
    }

    public static List<Id> getIds(List<SObject> sObjects) {
        final List<Id> idList = new List<Id>();
        for (SObject sObj : sObjects) {
            idList.add(sObj.Id);
        }
        return idList;
    }

}