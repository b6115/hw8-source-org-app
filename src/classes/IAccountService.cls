/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public interface IAccountService {
    void sendAll(List<Account> accounts);
}