/**
 * Created by alexanderbelenov on 29.05.2022.
 */

public with sharing class CredentialsDTO {
    private String clientId;
    private String clientSecret;
    private String username;
    private String password;
    private String securityToken;

    public CredentialsDTO(
            String clientId,
            String clientSecret,
            String username,
            String password,
            String securityToken) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.username = username;
        this.password = password;
        this.securityToken = securityToken;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getSecurityToken() {
        return this.securityToken;
    }

    public override String toString() {
        return 'ClientID: ' + this.getClientId() +
                '; ClientSecret: ' + this.getClientSecret() +
                '; Username: ' + this.getUsername() +
                '; Password: ' + this.getPassword() +
                '; SecurityToken: ' + this.getSecurityToken() +
                ';';
    }
}