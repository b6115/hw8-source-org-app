/**
 * Created by alexanderbelenov on 29.05.2022.
 */

public with sharing class CustomSettingService {
    public CredentialsDTO getTargetOrgCredentials(String username) {
        User user = [SELECT Id, Name FROM User WHERE name = :username LIMIT 1];
        TargetOrgCredentians__c credentials = TargetOrgCredentians__c.getInstance(user.Id);
        return new CredentialsDTO(
                credentials.client_id__c,
                credentials.client_secret__c,
                credentials.username__c,
                credentials.password__c,
                credentials.security_token__c
        );
    }
}