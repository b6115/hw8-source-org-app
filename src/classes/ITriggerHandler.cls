/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public interface ITriggerHandler {
    void afterInsert(List<SObject> element);
}