/**
 * Created by alexanderbelenov on 28.05.2022.
 */

public class AccountTriggerHandler implements ITriggerHandler {
    private final AccountService service = new AccountService();
    public void afterInsert(List<Account> accounts) {
        this.service.sendAll(accounts);
    }
}