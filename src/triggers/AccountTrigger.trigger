/**
 * Created by alexanderbelenov on 28.05.2022.
 */

trigger AccountTrigger on Account (
        before insert, before update, before delete,
        after insert, after update, after delete, after undelete) {
    final ITriggerHandler handler = new AccountTriggerHandlerFactory().create();

    if (Trigger.isAfter && Trigger.isInsert) {
        handler.afterInsert(Trigger.new);
    }
}